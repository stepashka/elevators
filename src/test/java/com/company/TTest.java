package com.company;

import com.company.builder.ElevatorBuilder;
import com.company.builder.PersonBuilder;
import com.company.exception.OutOfFloorNumberException;
import com.company.model.Elevator;
import com.company.model.Person;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class TTest {

    @Test(expectedExceptions = OutOfFloorNumberException.class, expectedExceptionsMessageRegExp = "Destination floor must be more than count of floors in building")
    public void testSetDestinationLessThanOneThrowException() throws IOException {
        new ElevatorBuilder().setDestination(0).build();
    }

    @Test(expectedExceptions = OutOfFloorNumberException.class, expectedExceptionsMessageRegExp = "Destination must be less than count of floors in building")
    public void testSetDestinationMoreThanNumberOfFloorsThrowException() throws IOException {
        new ElevatorBuilder().setDestination(101).build();
    }

    @Test
    public void testNumberOfFloor() throws IOException {
        assertThat(NumberOfFloors.get()).isEqualTo(100);
    }

    @Test
    public void testCurrentFloor() throws IOException {
        Elevator elevator = new ElevatorBuilder().setDestination(100).setCurrentFloor(3).build();
        assertThat(elevator.getCurrentFloor()).isEqualTo(3);
    }

    @Test
    public void testDestination() throws IOException {
        Elevator elevator = new ElevatorBuilder().setDestination(100).setCurrentFloor(3).build();
        assertThat(elevator.getDestination()).isEqualTo(100);
    }

    //crash
    @Test
    public void testPersonCurrentFloor() throws IOException {
        Person person = new PersonBuilder().setCurrentFloor(1).build();
        assertThat(person.getCurrentFloor()).isEqualTo(2);
    }

    //crash
    @Test
    public void testDestinationCrash() throws IOException {
        Elevator elevator = new ElevatorBuilder().setDestination(100).setCurrentFloor(3).build();
        assertThat(elevator.getDestination()).isEqualTo(101);
    }

    //crash
    @Test
    public void testPersonDestination() throws IOException {
        Person person = new PersonBuilder().setNecessaryDestination(Movement.UP).build();
        assertThat(person.getNecessaryDestination()).isEqualTo(Movement.DOWN);
    }

    // crash
    @Test
    public void testDefaultCurrentFloor() throws IOException {
        Person person = new PersonBuilder().setNecessaryDestination(Movement.UP).build();
        assertThat(person.getCurrentFloor()).isEqualTo(1);
    }

    //crash
    @Test
    public void testDefaultDestination() throws IOException {
        Person person = new PersonBuilder().setCurrentFloor(1).build();
        assertThat(person.getNecessaryDestination()).isEqualTo(Movement.STOP);
    }
}
