package com.company;

import com.company.builder.ElevatorBuilder;
import com.company.builder.PersonBuilder;
import com.company.model.Building;
import com.company.model.Elevator;
import com.company.model.Person;
import org.testng.annotations.AfterClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.testng.Assert.*;

public class BuildingTest {

    private ArrayList<Elevator> firstElevators = new ArrayList<>();
    private ArrayList<Elevator> secondElevators = new ArrayList<>();
    private ArrayList<Elevator> thirdElevators = new ArrayList<>();
    private ArrayList<Elevator> forthElevators = new ArrayList<>();
    private ArrayList<Elevator> fifthElevators = new ArrayList<>();
    private ArrayList<Elevator> sixthElevators = new ArrayList<>();

    private Elevator elevator1 = new ElevatorBuilder().setCurrentFloor(100).setDestination(99).build();
    private Elevator elevator2 = new ElevatorBuilder().setCurrentFloor(2).setDestination(96).build();
    private Elevator elevator3 = new ElevatorBuilder().setCurrentFloor(95).setDestination(94).build();
    private Elevator elevator4 = new ElevatorBuilder().setCurrentFloor(10).setDestination(1).build();
    private Elevator elevator5 = new ElevatorBuilder().setCurrentFloor(100).setDestination(2).build();
    private Elevator elevator6 = new ElevatorBuilder().setCurrentFloor(1).setDestination(2).build();
    private Elevator elevator7 = new ElevatorBuilder().setCurrentFloor(5).setDestination(3).build();
    private Elevator elevator8 = new ElevatorBuilder().setCurrentFloor(7).setDestination(4).build();
    private Elevator elevator9 = new ElevatorBuilder().setCurrentFloor(2).setDestination(2).build();
    private Elevator elevator10 = new ElevatorBuilder().setCurrentFloor(6).setDestination(3).build();
    private Elevator elevator11 = new ElevatorBuilder().setCurrentFloor(4).setDestination(2).build();
    private Elevator elevator12 = new ElevatorBuilder().setCurrentFloor(7).setDestination(9).build();
    private Elevator elevator13 = new ElevatorBuilder().setCurrentFloor(3).setDestination(4).build();
    private Elevator elevator14 = new ElevatorBuilder().setCurrentFloor(10).setDestination(5).build();
    private Elevator elevator15 = new ElevatorBuilder().setCurrentFloor(7).setDestination(9).build();
    private Elevator elevator16 = new ElevatorBuilder().setCurrentFloor(6).setDestination(8).build();
    private Elevator elevator17 = new ElevatorBuilder().setCurrentFloor(2).setDestination(3).build();
    private Elevator elevator18 = new ElevatorBuilder().setCurrentFloor(4).setDestination(3).build();


    private Person personFirst = new PersonBuilder().setCurrentFloor(3).setNecessaryDestination(Movement.UP).build();
    private Person personSecond = new PersonBuilder().setCurrentFloor(4).setNecessaryDestination(Movement.DOWN).build();
    private Person personThird = new PersonBuilder().setCurrentFloor(96).setNecessaryDestination(Movement.DOWN).build();
    private Person personForth = new PersonBuilder().setCurrentFloor(5).setNecessaryDestination(Movement.DOWN).build();
    private Person personFifth = new PersonBuilder().setCurrentFloor(5).setNecessaryDestination(Movement.UP).build();
    private Person personSixth = new PersonBuilder().setCurrentFloor(5).setNecessaryDestination(Movement.UP).build();

    private String temp = "";

    public BuildingTest() throws IOException {
    }

    @DataProvider
    public Object[][] elevatorProvider() {
        firstElevators.add(elevator5);
        firstElevators.add(elevator6);
        Building building1 = new Building(personFirst, firstElevators);
        secondElevators.add(elevator5);
        secondElevators.add(elevator7);
        secondElevators.add(elevator8);
        Building building2 = new Building(personSecond, secondElevators);
        thirdElevators.add(elevator1);
        thirdElevators.add(elevator2);
        thirdElevators.add(elevator3);
        thirdElevators.add(elevator4);
        Building building3 = new Building(personThird, thirdElevators);
        forthElevators.add(elevator9);
        forthElevators.add(elevator10);
        forthElevators.add(elevator11);
        Building building4 = new Building(personForth, forthElevators);
        fifthElevators.add(elevator12);
        fifthElevators.add(elevator13);
        fifthElevators.add(elevator14);
        Building building5 = new Building(personFifth, fifthElevators);
        sixthElevators.add(elevator15);
        sixthElevators.add(elevator16);
        sixthElevators.add(elevator17);
        sixthElevators.add(elevator18);
        Building building6 = new Building(personSixth, sixthElevators);

        return new Object[][]{{building1, elevator6}, {building2, elevator7},
                {building3, elevator2}, {building4, elevator10}, {building5, elevator14}, {building6, elevator17}};
    }

    @Test(dataProvider = "elevatorProvider")
    public void optimalElevatorTest(Building building, Elevator bestElevator) {
        long startTime = System.nanoTime();
        assertEquals(building.callElevator(), bestElevator);
        long endTime = System.nanoTime();
        long fullTime = endTime - startTime;
        temp += bestElevator.toString() + ", " + building.getPerson().toString() + " - " + fullTime + "\n";
    }


    @AfterClass
    public void getTime() {
        Writer.writeInFile("testsTime.txt", temp);
    }
}