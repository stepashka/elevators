package com.company.model;

import com.company.Movement;

public class Person {

    private int currentFloor;
    private Movement necessaryDestination;

    public Person() {
    }

    public Person(int currentFloor, Movement necessaryDestination) {
        this.currentFloor = currentFloor;
        this.necessaryDestination = necessaryDestination;
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public Movement getNecessaryDestination() {
        return necessaryDestination;
    }

    @Override
    public String toString() {
        return "Person{" +
                "currentFloor=" + currentFloor +
                ", necessaryDestination=" + necessaryDestination +
                '}';
    }
}