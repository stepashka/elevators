package com.company.model;

import com.company.Movement;

import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class Building {

    private Person person;
    private List<Elevator> elevators;

    public Building() {}

    public Building(Person person, List<Elevator> elevators) {
        this.person = person;
        this.elevators = elevators;
    }

    public Person getPerson() {
        return person;
    }

    private List<Elevator> availableElevators() {
         return (person.getNecessaryDestination().equals(Movement.UP)) ?
              elevators.stream().filter(el -> el.getState().equals(Movement.UP)
                     && el.getCurrentFloor() < person.getCurrentFloor()
                     && el.getDestination() > person.getCurrentFloor()).collect(Collectors.toList())
                 : elevators.stream().filter(el -> el.getState().equals(Movement.DOWN)
                    && el.getCurrentFloor() > person.getCurrentFloor()
                    && el.getDestination() < person.getCurrentFloor()).collect(Collectors.toList());
    }

    private List<Elevator> stoppingOnPersonsFloor() {
        return elevators.stream()
                .filter(el -> el.getDestination() == person.getCurrentFloor() && !el.getState().equals(Movement.STOP))
                .collect(Collectors.toList());
     }

    private List<Elevator> standingOnPersonsFloor() {
        return elevators.stream().filter(el -> el.getState().equals(Movement.STOP)
                && el.getCurrentFloor() == person.getCurrentFloor()).collect(Collectors.toList());
    }


    private Elevator minDistanceForPersonInInterval() {
        return availableElevators()
                .stream()
                .min(Comparator.comparing(el -> Math.abs(person.getCurrentFloor() - el.getCurrentFloor())))
                .orElseThrow(NoSuchElementException::new);
    }

    private Elevator minDistance() {
        return stoppingOnPersonsFloor().stream()
                .min(Comparator.comparing(el -> el.getCurrentFloor() - person.getCurrentFloor()))
                .orElseThrow(NoSuchElementException::new);
    }

    private Elevator getElevatorWithSmallestDistance() {
        return  elevators
                .stream()
                .min(Comparator
                        .comparing(el -> el.getDistance() + Math.abs(el.getDestination() - person.getCurrentFloor())))
                .orElseThrow(NoSuchElementException::new);
    }

    public Elevator callElevator() {
        if (!availableElevators().isEmpty()) {
            return minDistanceForPersonInInterval();
        }
        if (!stoppingOnPersonsFloor().isEmpty()) {
            return minDistance();
        }
        if (!standingOnPersonsFloor().isEmpty()) {
            return standingOnPersonsFloor().stream().findFirst().orElseThrow(NoSuchElementException::new);
        }
        else {
            return getElevatorWithSmallestDistance();
        }
    }
}