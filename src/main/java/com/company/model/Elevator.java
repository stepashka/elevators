package com.company.model;

import com.company.Movement;

public class Elevator {

    private int currentFloor;
    private int destination;
    private Movement state;

    public Elevator() {
    }

    public Elevator(int currentFloor, int destination) {
        this.currentFloor = currentFloor;
        this.destination = destination;
        this.state = movementValidation(currentFloor, destination);
    }

    private Movement movementValidation(int currentFloor, int destination) {
        if (currentFloor > destination)
            return Movement.DOWN;
        else if (currentFloor < destination)
            return Movement.UP;

        return Movement.STOP;
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public int getDestination() {
        return destination;
    }

    public Movement getState() {
        return state;
    }

    public int getDistance() {
        return Math.abs(destination - currentFloor);
    }

    @Override
    public String toString() {
        return "Elevator{" +
                "currentFloor=" + currentFloor +
                ", destination=" + destination +
                ", state=" + state +
                '}';
    }
}