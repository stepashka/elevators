package com.company;

import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Writer {

    private static final Logger LOGGER =  Logger.getLogger(Writer.class.getName());

    private Writer() {
    }

    public static void writeInFile(String fileName, String inputData) {
        try (FileWriter fileWriter = new FileWriter(fileName, false)){
            fileWriter.append(inputData).append("\n");
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
        }
    }
}
