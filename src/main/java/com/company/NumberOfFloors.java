package com.company;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class NumberOfFloors {

    private NumberOfFloors() {
    }

    public static int get() throws IOException{
            File file = new File("floor.properties");
            FileInputStream fileInputStream = new FileInputStream(file);
            Properties properties = new Properties();
            properties.load(fileInputStream);
            fileInputStream.close();

     return Integer.parseInt(properties.getProperty("maxFloor"));
    }
}
