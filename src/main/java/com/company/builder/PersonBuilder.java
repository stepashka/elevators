package com.company.builder;


import com.company.Movement;
import com.company.exception.MovementException;
import com.company.NumberOfFloors;
import com.company.exception.OutOfFloorNumberException;
import com.company.model.Person;

import java.io.IOException;

public class PersonBuilder {

    private int currentFloor;
    private Movement necessaryDestination;

    public PersonBuilder setCurrentFloor(int currentFloor) throws IOException {
        if (currentFloor < 1) throw new OutOfFloorNumberException("Current floor must be more than 0 !!!");
         else if (currentFloor > NumberOfFloors.get())
             throw new OutOfFloorNumberException("Current floor must be less than number of max floor !!!");
        else {
            this.currentFloor = currentFloor;
            return this;
        }
    }

    public PersonBuilder setNecessaryDestination(Movement necessaryDestination) throws IOException{
        if (necessaryDestination.equals(Movement.UP) && currentFloor == NumberOfFloors.get())
            throw new MovementException("Movement of person must be downwards, because current floor is the last.");
        else if (necessaryDestination.equals(Movement.DOWN) && currentFloor == 1)
            throw new MovementException("Movement of person must be upwards, because current floor is the first.");
        else if (necessaryDestination.equals(Movement.STOP))
            throw new MovementException("Movement of person can't be STOP!!!");
        else {
            this.necessaryDestination = necessaryDestination;
            return this;
        }
    }

    public Person build() {
        return new Person(currentFloor, necessaryDestination);
    }
}
