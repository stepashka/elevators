package com.company.builder;

import com.company.NumberOfFloors;
import com.company.exception.OutOfFloorNumberException;
import com.company.model.Elevator;

import java.io.IOException;

public class ElevatorBuilder {

    private int currentFloor;
    private int destination;

    public ElevatorBuilder setCurrentFloor(int currentFloor) throws IOException {
        if (currentFloor > NumberOfFloors.get())
            throw new OutOfFloorNumberException("Current floor must be less than count of floors in building");
        else if (currentFloor < 1)
            throw new OutOfFloorNumberException("Current floor must be more than count of floors in building");
        else {
            this.currentFloor = currentFloor;
            return this;
        }
    }

    public ElevatorBuilder setDestination(int destination) throws IOException{
        if (destination > NumberOfFloors.get())
            throw new OutOfFloorNumberException("Destination must be less than count of floors in building");
        else if (destination < 1)
            throw new OutOfFloorNumberException("Destination floor must be more than count of floors in building");
        else {
            this.destination = destination;
            return this;
        }
    }

    public Elevator build() {
        return new Elevator(currentFloor, destination);
    }


}
