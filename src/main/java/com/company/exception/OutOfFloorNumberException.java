package com.company.exception;

public class OutOfFloorNumberException extends RuntimeException {

    public OutOfFloorNumberException(String message) {
        super(message);
    }

}
