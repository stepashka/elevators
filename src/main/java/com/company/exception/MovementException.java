package com.company.exception;

public class MovementException extends RuntimeException {

    public MovementException(String message) {
        super(message);
    }
}